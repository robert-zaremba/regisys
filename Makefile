CHECK=\033[32m✔\033[39m
HR=\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#
VENDOR_JS=static/js/vendor.min.js
B_CSS=static/css/bootstrap.css

IBLACK='\e[1;30m'
GREEN='\e[0;32m'
NC='\e[0m'              # No Color

run:
	cd app; python run.py

run-prod:
	cd app; /path/to/env/python run.py

mongo:
	sudo systemctl start mongodb
#	mongod --config /etc/mongodb.conf


###############################
# watchig/compilint assets
#   for this you need to install node components:
#   npm install -g less jshint recess uglify-js

vendor-js: dir-js
	@echo -e "${IBLACK} Building and minifying javascript...${NC}"
	@cat assets/js/jquery-1.8.3.min.js  >  ${VENDOR_JS}
	@echo "/**\n* Bootstrap.js v2.2.2 by @fat & @mdo\n* Copyright 2012 Twitter, Inc.\n* http://www.apache.org/licenses/LICENSE-2.0.txt\n*/"  >>  ${VENDOR_JS}
	@uglifyjs assets/js/bootstrap-*.js  >> ${VENDOR_JS} # TODO use -m for more compression
	@cat assets/js/angular.min.js  >>  ${VENDOR_JS}
	@echo -e "   ${CHECK} Done"

dir-js:
	@echo -e "${IBLACK} checking static dir... ${NC}"
	@if [ ! -d static/js ]; then echo -e "${IBLACK}making static/js directory${NC}    ${CHECK} Done"; mkdir -p static/js; fi;


css: css-bootstrap css-responsive

css-bootstrap: dir-css
#	@recess --compile  assets/less/bootstrap.less   >  static/css/bootstrap.css
	@recess --compress assets/less/bootstrap.less   >  static/css/bootstrap.min.css
	@echo -e "${IBLACK}Compiling bootstrap.less with recess...${NC}               ${CHECK} Done"

css-responsive: dir-css
#	@recess --compile  assets/less/responsive.less  >  static/css/bootstrap-responsive.css
	@recess --compress assets/less/responsive.less  >  static/css/bootstrap-responsive.min.css
	@echo -e "${IBLACK}Compiling responsive.less with recess...${NC}               ${CHECK} Done"

dir-css:
	@if [ ! -d static/css ]; then echo -e "${IBLACK}making static/css directory${NC}    ${CHECK} Done"; mkdir -p static/css; fi;


watch:
	@echo -e "${IBLACK}starting watcher...${NC}"
	@python2 watcher.py start
	@if [ ! -f ~/.watcher/jobs.yaml ]; then echo -e "   edit '~/.watcher/jobs.yaml' file and start again"; else echo -e "               ${CHECK} Done"; fi;

watch-stop:
	python2 watcher.py stop

watch-restart:
	python2 watcher.py restart

