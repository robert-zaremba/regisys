import tornado.web
import logger

import config
from base import Application
import controllers

log  = logger.make_logger(config.name, debug=config.debug, colored=config.log_colored, force=True)
logD = logger.make_logger(config.name+'D', debug=config.debug, colored=config.log_colored, force=True)

application = Application([
    (r"/testdb", controllers.TestDB),
    (r"/testsession", controllers.TestSession),
    (r"/(.*)", controllers.Main),
    (r"/(apple-touch-icon\.png)", tornado.web.StaticFileHandler, {'path': '../static'})  # /favicon.ico and /robots.txt is automaticly handled by tornado
    ],
    default_host=config.hostname,
    debug=config.debug,
    template_path = '../views',
    static_path = '../static',
    xsrf_cookies = True,
    cookie_secret = 'secret83fca550ef89f26dc4a76502e2f93b91',
    xheaders = config.env == config.Env.prod,
)
