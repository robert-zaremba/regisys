import json
import time
from uuid import uuid4
from bson import ObjectId
from datetime import datetime
import logging
from tornado.web import asynchronous
from tornado import gen
from pprint import pformat

import config
log = logging.getLogger(config.name)


class MongoSession(object):
    @staticmethod
    def generate_sid():
        return str(ObjectId())

    def __init__(self, col, sid, callback, expire=7200):
        """@param col: MongoDB collection object
        @param expire: session living time in seconds
        @type expire: int
        @param connection: mongo db object
        """
        self.col = col
        self._sid = sid
        self.dirty = False
        self.callback = callback
        col.find_one(sid, callback=self._on_session)

    def _on_session(self, result):
        """callback for __init__ method"""
        (self._doc, err) = result
        if self._doc is None:
            self._doc = {'_id': self.sid,
                         'data': {},
                         'timestamp': datetime.now()}
            self.col.insert(self._doc)
        self._data = self._doc['data']

        self.__getitem__ = self._data.__getitem__
        self.__len__  = self._data.__len__
        self.__iter__ = self._data.__iter__
        self.__repr__ = self._data.__repr__
        self.__contains__ = self._data.__contains__
        self.__repr__ = self._data.__repr__
        callback = self.callback
        del self.callback
        callback(self)

    def __str__(self):
        return pformat(self._data)

    def __setitem__(self, key, value):
        self._data[key] = value
        self.dirty = True

    def __delitem__(self, key):
        del self._data[key]
        self.dirty = True

    @property
    def sid(self):
        """the session id"""
        return self._sid

    def save(self):
        if self.dirty:
            self.save_force()

    def save_force(self):
        log.debug('saveing session')
        self._doc['timestamp'] = datetime.now()
        self.col.update(self._sid, self._doc)
        self.dirty = True

    def remove(self):
        self.col.remove(self._sid)

    def touch(self, remote_ip):
        """update timestamp"""
        now = datetime.now()
        self.col.update(self._sid, {'$set': {'timestamp': now, 'ip': remote_ip}})
        self._doc['timestamp'] = now
        self._doc['ip'] = remote_ip

    def last_access(self):
        return self._doc['timestamp']


class RedisSyncSession(object):
    session_key = 'session'

    @staticmethod
    def generate_sid():
        return uuid4().get_hex()

    def __init__(self, connection, sid, expire=7200):
        """@param expire: session living time in seconds
        @type expire: int
        @param connection: redis connection object
        """
        self.expire = expire
        self.redis = connection
        self.sid = sid
        data = self.redis.hget(self.session_key, sid)
        self._data = json.loads(data) if data else {}

    def save(self):
        self.redis.hset(self.key_prefix, sid,
                        json.dumps(self._data, separators=(',',':')))
        if self.expiry:
            self.redis.expire(self.prefixed(sid), self.expiry)

    def delete(self, sid):
        self.redis.delete(self.prefixed(sid))

    def touch(self, remote_ip):
        self.redis.set(self.sid,
            'last_access',
            json.dumps({'remote_ip':remote_ip, 'time':'%.6f' % time.time()},
                       separators=(',',':')))

    def last_access(self):
        access_info = self.redis.get(self.sid, 'last_access')
        return json.loads(access_info)
