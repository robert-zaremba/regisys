from tornado.web import asynchronous, RequestHandler
from tornado.gen import Task, engine
import logging

import config
from base import Request, with_session
log = logging.getLogger(config.name)


class Main(Request):
    def get(self, name):
        name = name or "index"
        self.render(name+".html") # this will also call finish


class TestSession(Request):
    @with_session
    @engine
    def get(self):
        if self.session is None:
            yield Task(self.init_session)
        self.session['args'] = self.request.arguments
        # col = self.application.db.session
        # from bson import ObjectId
        # col.insert({'_id': '3', 'timestamp': '3'})#datetime.utcnow()})
        # col.update('3', {'timestamp': datetime.utcnow()})
        self.write("working with session<br /> session: %s" % self.session)
        self.finish()


class TestDB(RequestHandler):
    @asynchronous
    @engine
    def get(self):
        col = self.application.db.test
        print 'inserting user'
        user = {'_id': 1, 'name': 'User Name'}
        a = yield Task(col.insert, user, False)
        print 'user inserted',  a
        yield Task(col.update, user['_id'],
                       {"$set": {'name': 'New User Name'}})
        user_found = yield Task(col.find_one, user['_id'])
        yield Task(col.remove, user['_id'])

        self.write("Hello, world %s" % (user_found,))

        found, err = yield Task(col.find_one, user['_id'])
        assert found is None and err is None
        self.finish()
