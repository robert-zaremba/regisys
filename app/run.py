import argparse
import tornado.ioloop

import config


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Start tornado web applicatoin')
    parser.add_argument('--debug', action='store_true',
                        help='set debug mode')
    parser.add_argument('--port', type=int,
                        help='set http server port number')
    args = parser.parse_args()

    if args.debug:
        config.debug = True
    if args.port:
        config.port = args.port

    from app import application, log

    application.listen(config.port)
    log.info('Starting tornado server on {} port. Debug: {}'.format(config.port, config.debug))
    tornado.ioloop.IOLoop.instance().start()
