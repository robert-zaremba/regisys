from urlparse import urlparse
import functools
import logging
from mongotor.database import Database
import tornado.web
from tornado import stack_context

import config
import models
from session import MongoSession as Session
log  = logging.getLogger(config.name)
logD = logging.getLogger(config.name+'D')


class Application(tornado.web.Application):
    def __init__(self, handlers, **settings):
        tornado.web.Application.__init__(self, handlers, **settings)

        db_url = urlparse(config.database)
        assert db_url.path[0] == '/'
        self.db = Database.connect(db_url.netloc, db_url.path[1:])
        self.model = models.Context(self.db)
        self.session_store = self.db.session
        self.session_store.ensure_index( [('timestamp', 1),], name='ttl', expireAfterSeconds=7200)  # 2h


class Request(tornado.web.RequestHandler):
    def initialize(self):
        self.model = self.application.model
        self.session = None
        self._callback = None

    def get_current_user(self):
        """needed by current_user property and authenticated decorator"""
        return self.session['user'] if self.session and 'user' in self.session else None

    def init_session(self, callback):
        assert self._callback is None
        self._callback = callback
        log.debug('Initialising session, setting cookie')
        sid = Session.generate_sid()
        self.set_secure_cookie('sid', sid)
        Session(self.application.session_store, sid, self._on_session)

    def _on_session(self, session):
        logD.debug("Session received")
        self.session = session
        callback = self._callback
        self._callback = None
        callback()

    def on_finish(self):
        logD.debug("Finishing request")
        if self.session is not None:
            self.session.save()  # TODO maybe save_force is better?


def with_session(method):
    """Makes the method asynchronous and add session support"""
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if self.application._wsgi:
            raise Exception("@asynchronous is not supported for WSGI apps")
        self._auto_finish = False
        with stack_context.ExceptionStackContext(
            self._stack_context_handle_exception):
            sid = self.get_secure_cookie('sid')
            if sid is not None:
                logD.debug('Makeing session object')
                def callback(session):
                    self.session = session
                    return method(self, *args, **kwargs)
                Session(self.application.session_store, sid, callback)
            else:
                return method(self, *args, **kwargs)
    return wrapper
