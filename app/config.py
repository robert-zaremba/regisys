class Env:
    dev = 'dev'
    prod = 'prod'

name = 'regisys'
port = 8000
env  = Env.dev
hostname = 'localhost'
debug = True
log_colored = True
database = 'mongodb://localhost:27017/regisys'

# try to overwrite your production configuration with private one witch comes
# from your non-versioned private configuration
try:
    from private_config import *
except ImportError:
     pass
